"use strict";
var Q   = require('q');

// Manage finding and creating Entity objects of `type` in remote STA.
//
// In `findOrCreate` and `create` the model passed in will be passed as-is to
// SensorThings. The model should be properly serialized and validated before
// being passed into those functions.
class EntityService {
  constructor(type, options) {
    this.request = options.request;
    this.type    = type;
    this.url     = options.url;

    if (options.log) {
      this.log = options.log;
    } else {
      this.log = function() {};
    }
  }

  create(entity) {
    var deferred = Q.defer();
    this.log(`Creating new ${this.type}.`);

    this.request.post(this.url, entity)
    .then((response) => {
      var newEntity = response.body;
      this.log(newEntity);
      deferred.resolve(newEntity);
    }, function(reason) {
      deferred.reject(new Error(reason));
    })
    .done();

    return deferred.promise;
  }

  // Execute create for each entity
  createAll(entities) {
    var promises = Q.all(entities.map((entity) => {
      return this.create(entity);
    }));

    return promises;
  }

  // Lookup `entity` in STA. Will use `filter` to try to match existing
  // entities.
  // If more than one entity matches, then `comparator` will be used to try to
  // match a single entity.
  //
  // Returns a promise object.
  //
  // Promise will resolve with a remote copy if it already exists.
  // If none exist on remote, then a null value is returned.
  // If more than one exists, try to match using `comparator` (should be
  // unique), and resolve with that entity.
  //
  // If more than one comparator matches, then it will reject the promise.
  find(entity, filter, comparator) {
    return this._find(entity, filter, comparator, () => {
      return null;
    });
  }

  // Lookup entities in STA that match `filter`, following pagination up to
  // `limit`. If more than `limit` are collected, then the results will be
  // truncated to `limit` items.
  findAll(filter, limit) {
    if (limit === undefined) {
      limit = 10000;
    }

    this.log(`Finding all ${this.type} matching a filter up to ${limit}.`);

    var deferred = Q.defer();
    var that     = this;
    var entities = [];

    // recursively keep retrieving and collecting items until collection limit
    // or `limit` argument is passed.
    var subGet = function(url) {
      that.request.query({
        json: true,
        uri : url
      })
      .then((response) => {
        var body        = response.body;
        var resultCount = body["@iot.count"];
        entities        = entities.concat(body.value);

        if (entities.length < resultCount && entities.length < limit) {
          that.log(`FindAll: ${entities.length} of ${resultCount}, limiting to ${limit}`);
          subGet(body["@iot.nextLink"]);
        } else {
          if (entities.length > limit) {
            entities = entities.slice(0, limit);
          }
          that.log(`FindAll: ${entities.length} returned`);
          deferred.resolve(entities);
        }
      })
      .done();
    };

    subGet(this.url + "?$filter=" + encodeURIComponent(filter));

    return deferred.promise;
  }

  // Lookup `entity` in STA. Will use `filter` to try to match existing
  // entities.
  // If more than one entity matches, then `comparator` will be used to try to
  // match a single entity.
  //
  // Returns a promise object.
  //
  // Promise will resolve with a remote copy if it already exists.
  // If none exist on remote, create one then resolve with that.
  // If more than one exists, try to match using `comparator` (should be
  // unique), and resolve with that entity.
  //
  // If more than one comparator matches, then it will reject the promise.
  findOrCreate(entity, filter, comparator) {
    return this._find(entity, filter, comparator, (entity) => {
      return this.create(entity);
    });
  }

  // Lookup `entity` in STA. Will use `filter` to try to match existing
  // entities.
  // If more than one entity matches, then `comparator` will be used to try to
  // match a single entity.
  //
  // Returns a promise object.
  //
  // Promise will resolve with a remote copy if it already exists.
  // If none exist on remote, then the `action` callback will be triggered with
  // the value of `entity`.
  // If more than one exists, try to match using `comparator` (should be
  // unique), and resolve with that entity.
  //
  // If more than one comparator matches, then it will reject the promise.
  _find(entity, filter, comparator, action) {
    this.log(`Searching for ${this.type}`);

    var deferred = Q.defer();

    this.request.get(this.url, {
      "$filter": filter
    })
    .then((response) => {
      var body = response.body;
      this.log(body);
      var resultCount = body["@iot.count"];

      if (resultCount === 0) {
        this.log(`No matching ${this.type} found`);
        deferred.resolve(action(entity));

      } else if (resultCount === 1) {
        this.log(`One matching ${this.type} found.`);
        deferred.resolve(body.value[0]);

      } else if (resultCount > 1) {
        this.log(`More than one matching ${this.type} found.`);

        if (comparator === undefined) {
          deferred.reject(new Error(`No comparator for ${this.type} defined.`));
        }

        var entities = body.value;
        var matches = entities.filter(comparator);

        if (matches.length === 0) {
          deferred.resolve(this.create(entity));
        } else if (matches.length === 1) {
          deferred.resolve(matches[0]);
        } else {
          deferred.reject(new Error(`Too many matching ${this.type}`));
        }

      } else {
        // result count is not 0 or greater. fail. Usually caused by an
        // unexpected response from STA.
        deferred.reject(new Error(`${this.type} result count invalid: ` + resultCount));
      }

    }, function(reason) {
      deferred.reject(new Error(reason));
    })
    .done();

    return deferred.promise;
  }
}

module.exports = EntityService;
