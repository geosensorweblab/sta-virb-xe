"use strict";
var Q = require('q');
var CreateObservationsService = require('./sensor_things/create_observations_service');
var EntityService = require('./sensor_things/entity_service');

// Manage interactions with a SensorThings API.
//
// options:
//   log - method to call when logging debug messages. Omit to disable logging.
//   request - Request object that will handle get and post requests to
//             SensorThings API.
//   url - URL of SensorThings API.
class SensorThings {
  constructor(options) {
    this.request = options.request;
    this.url = options.url;

    if (options.log) {
      this.log = options.log;
    } else {
      this.log = function() {};
    }
  }

  // Return a new CreateObservationsService.
  // For SensorThings that supports the DataArray extension.
  // options:
  //   url - URL to a CreateObservations resource in STA
  createObservationsService(url) {
    return new CreateObservationsService({
      log:     this.log,
      request: this.request,
      url:     url || (this.url + "CreateObservations")
    });
  }

  // Return a new DatastreamService.
  // options:
  //   url - URL to a Datastreams collection in STA
  datastreamService(url) {
    return this._newService("Datastream", url || (this.url + "Datastreams"));
  }

  // Return a new LocationService.
  // options:
  //   url - URL to a Locations collection in STA
  locationService(url) {
    return this._newService("Location", url || (this.url + "Locations"));
  }

  // Return a new ObservationService.
  // options:
  //   url - URL to a Observations collection in STA
  observationService(url) {
    return this._newService("Observation", url || (this.url + "Observations"));
  }

  // Return a new ObservedPropertyService.
  // options:
  //   url - URL to a ObservedProperties collection in STA
  observedPropertyService(url) {
    return this._newService("ObservedProperty", url || (this.url + "ObservedProperties"));
  }

  // Return a new SensorService.
  // options:
  //   url - URL to a Sensors collection in STA
  sensorService(url) {
    return this._newService("Sensor", url || (this.url + "Sensors"));
  }

  // Return a new ThingService.
  // options:
  //   url - URL to a Things collection in STA
  thingService(url) {
    return this._newService("Thing", url || (this.url + "Things"));
  }

  _newService(name, url) {
    return new EntityService(name, {
      log:     this.log,
      request: this.request,
      url:     url
    });
  }
}

module.exports = SensorThings;
