# SensorThings Garmin Virb XE

This repo will allow publishing the sensor outputs of a Garmin Virb XE to SensorThings. It will also allow publishing the RTSP stream as an HTTP stream if command-line VLC is installed.

## Setting Up Entities

Use the initialization script to create the base entities in SensorThings. It is idempotent, so it is safe to run it multiple times as it will re-use existing entities if they are present.

### Requirements

* Node 5+

### Usage

Run the init script with the URL to SensorThings to initialize the entities.

    $ node bin/init.js http://example.com/OGCSensorThings/v1.0/

If an output directory is specified, copies of the entities will be stored as JSON files.

    $ node bin/init.js http://example.com/OGCSensorThings/v1.0/ -o sta-cache

## Publishing HTTP Stream

The camera supports a one-user RTSP stream. We can use that stream with VLC to re-stream as a multi-user HTTP stream.

### Requirements

* [VLC](http://www.videolan.org/vlc/#download)
* OR [vlc-nox](http://askubuntu.com/questions/429693/what-is-vlc-nox)

### Usage

Run the stream script with the RTSP URL, and access on port 7000:

    $ bin/stream rtsp://192.168.1.2/livePreviewStream

Then in VLC on your client, open the network URL http://192.168.1.3:7000/, or whichever IP is hosting the VLC streaming server.

To publish a new observation to the Datastream for the HTTP server, edit the `sta-templates/observation-http-server.json` with the latest date and the result should point to the URL of the HTTP streaming service. Include a trailing slash if the server requires it. Then publish the Observation using curl:

    $ curl -d @sta-templates/observation-http-server.json -H "Content-Type: application/json" http://example.com/OGCSensorThings/v1.0/Datastreams(1)/Observations

The URL should point to the Observations collection for the HTTP Server datastream.

## Publishing Sensor Observations

Run the publish script with the URL to the Virb API and to the SensorThings API:

    $ node bin/publish.js http://192.168.1.2/virb http://example.com/OGCSensorThings/v1.0/

This will automatically create observations in SensorThings for the %%%%% sensors.

## License

GPL License (http://www.gnu.org/licenses/gpl-3.0.en.html)
